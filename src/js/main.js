var myApp = angular.module("myApp", ["ngRoute"]);
//
// myApp.directive("myDir");
//
// myApp.run(function(myOtherApp) {
//   angular.module("myOtherApp");
// });
//


// We are injecting the directives as module dependencies in order
// to get access to services and rss

myApp.controller("mainController",["$scope", "$log", "$filter", "$http", function ($scope, $log, $filter, $http) {

    //var ctrl = this;
    this.name ="John";

    $scope.food ="";

    this.lowercase = function() {
         return $filter("lowercase")(name);
      };
   //
   //  ctrl.$log(name + $resource);
   //
    $scope.characters = 5;
    //this.characters = 6;
   // ctrl.characters =

    // $scope.rules = [
    //     { rulename: "Must be 5 characters"},
    //     { rulename: "Must contain a Capital Letter"},
    //     { rulename: "Must contain 1 Number"}
    //
    // ]

   // var rulerequest = new XMLHttpRequest();

    // rulerequest.onreadystatechange = function() {
    //     // wrapping the function into the angular scope
    //     $scope.apply(function() {
    //         if(rulerequest.readyState == 4 && rulerequest.status == 200) {
    //             $scope.rules =JSON.parse(rulerequest.responseText);
    //         }
    //     })
    // };
    // rulerequest.open("GET", "",true);
    // rulerequest.send();

}]);

// Helper function from jsfiddle BinaryMuse
// that allows us to slice arrays
myApp.filter('split', function() {
    return function(arr, splitChar) {
        return arr.split(splitChar);

    };
});



// We are going to use the angular way of making xtml req
myApp.controller('dbCtrl', ['$scope', '$http', "$filter", function ($scope, $http, $filter) {

        //$scope.tempArr = [];

    //$scope.$apply(function() {
    $http.get("dal2.php")
            .success(function(data) {
    
                $scope.rules = [data];
                $scope.ruleRegex = '"';//'"[a-z]{8}":';
    
                for (var rule in $scope.rules) {
        
                    var json_string = JSON.parse(JSON.stringify(data[rule]));
                    //console.log(json_string) | $scope.ruleRegex;
                    var temp = [];
                    temp.push(rule);
        
                }
                console.log(temp);

                // split the result at commas
                $scope.mySplit = function(string, nb) {
                    var temp = json_string.split('"');
                    var array = temp.splice(0);
                    //console.log(array[nb]);
                    return array[nb];
                }
            })
            .error(function(data, status) {
                console.log("error in fetching data" + data);
            });
    
    
    $scope.newRule = "";
    $scope.addRule = function () {
        // TODO: some validation
        
        // $http({
        //     method:"POST",
        //     url: "dal1.php",
        //     data: $scope.newRule
        // });
        
        
        // post data as json
        $http.post("dal1.php", {newRule: $scope.newRule})
        .success(function (result) {
            // update our rules
            //$scope.rules.push(result);
            $scope.rules = result;
            console.log(result);
            //clear input box
            
        })
        .error(function (data, status) {
            console.log("error in fetching data" + data);
        });
        $scope.newRule = "";
    };


    //});



 /*
  // split the result at commas
  $scope.mySplit = function(string, nb) {
  var array = (json_string.split(','));
  //console.log(array[nb]);
  return array[nb];

  } */
    
    
    
    $scope.saveUser = function (type) {
        var data = $scope.param({
            'data': $scope.newRule,
            'type': type
        });
        var config = {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
            }
        };
        $http.post("dal1.php", data, config).success(function (response) {
            if (response.status == 'OK') {
                $scope.rules.push({
                    id: response.data.id,
                    rulename: response.data.rulename
                });
                this.messageSuccess("Well done");
            }
            else {
                this.messageError("error:" + response.result)
            }
            $scope.newRule.$setPristine();
            $scope.newRule = {};
            
        });
    };
    
    
    
    // $scope.messageSuccess = function(msg){
    //     $('.alert-success > p').html(msg);
    //     $('.alert-success').show();
    //     $('.alert-success').delay(5000).slideUp(function(){
    //         $('.alert-success > p').html('');
    //     });
    // };
    //
    // // function to display error message
    // $scope.messageError = function(msg){
    //     $('.alert-danger > p').html(msg);
    //     $('.alert-danger').show();
    //     $('.alert-danger').delay(5000).slideUp(function(){
    //         $('.alert-danger > p').html('');
    //     });
    // };
    
    
    
}]);

//
// //Second module
// var multiModule = angular.module("multiModule", ["ngRoute"]);
//
// // Remember to config your routes with the provider service
// // multi.config(function($routeProvider) {
// //     $routeProvider
//
// (function (multiModule) {
//
//     var routeConfig =  function(ngRoute, $routeProvider) {
//       $routeProvider
//       //condition1: app root, index
//           .when("/", {
//               templateUrl: "index.html",
//               controller: "mainController"
//           })
//           //condition2
//           .when("/AngularJS101/src/", {
//               templateUrl: "lorem_ipsum.html",
//               controller: "mainController"
//           })
//           // Fall back path
//           .otherwise("/AngularJS101/src/")
//     };
//     routeConfig.$inject = ['$routeProvider'];
//     multiModule.config(routeConfig);
// } // end function
// )(angular.module("multiModule"));
//
//
//
//
//
// multiModule.controller("mainController",
//     ["$scope", "$location", "$log",
//         function ($scope, $location, $log) {
//
//     $log.info($location.path());
//
// }]);