/**
	* Created by FreshEyes on 16-10-2016.
	*/

var myOtherApp = angular.module("myOtherApp", ["ngRoute"]);

myOtherApp.config(function ($routeProvider) {
		$routeProvider
		.when("/Testing", {
				templateUrl: "tpl/Testing.html",
				controller: "mainController"
		})
		
		.when("/lorem_ipsum", {
				templateUrl: "tpl/lorem_ipsum.html",
				controller: "mainController"
		})
		
		.when("/lorem_ipsum/:num", {
				templateUrl: "tpl/lorem_ipsum.html",
				controller: "secondController"
		})
		.when("/directives", {
				templateUrl: "tpl/directives.html",
				controller: "mainController"
		})
		.when("/index", {
				templateUrl: "tpl/Testing.html",
				controller: "mainController"
		});
		// .otherwise ({redirectTo:"index"
		// });
});

// Creating our service/singleton object
myOtherApp.service("nameService", function () {
		var self = this;
		this.title = "Hi from the nameservice";
		
		this.namelength = function () {
				return self.title.length;
		}
		
});

myOtherApp.controller("mainController",
		["$scope", "$location", "$log", "nameService",
				function ($scope, $location, $log, nameService) {
						
						// $log.info($location.path());
						// $scope.name = nameService.title;
						//
						// // we need to set up a listener for changes
						// $scope.$watch("name", function() {
						//     // and update the scope variable/value accordingly
						//     nameService.title = $scope.name;
						// });
						//
						// $log.log(nameService.name);
						// $log.log(nameService.namelength());
						
						$scope.people = [
								{
										name: "James Dean",
										address: "55 Main St",
										city: "New York",
										state: "NY",
										zip: "1111"
								},
								{
										name: "Marc Dean",
										address: "57 Main St",
										city: "Miami",
										state: "FL",
										zip: "5555"
								},
								{
										name: "Michael Dean",
										address: "59 Main St",
										city: "New York",
										state: "NY",
										zip: "1111"
								},
								{
										name: "Marilyn Monroe",
										address: "54 Second St",
										city: "Buffalo",
										state: "NY",
										zip: "2222"
								},
								{
										name: "Vanessa Paradis Depp ",
										address: "56 Main St",
										city: "New York",
										state: "NY",
										zip: "1111"
								}
						
						
						];
						
						$scope.formattedAddress = function (personObj) {
								return personObj.address + ", " +
										personObj.city + ", " +
										personObj.state + ", " +
										personObj.zip;
						}
						
				}]);


myOtherApp.controller("secondController",
		["$scope", "$log", "$location", "$routeParams", "nameService",
				function ($scope, $log, $location, $routeParams, nameService) {
						
						$log.info($location.path());
						//$scope.person = nameService.title;
						
						$scope.num = $routeParams.num || 1;
						
						$scope.$watch("name", function () {
								nameService.title = $scope.name;
						});
						
				}]);

// Be aware not to let a directive impact the DOM too much. Use it to encapsulate functionality
myOtherApp.directive("searchResult", function () {
		return {
				restrict: "AECM",
				templateUrl: "tpl/directives.html",
				replace: true, // will generate the appropriate html everywhere this custom element is found : defaults to false
				
				scope: { // new model: isolating the model-part of the directive(person) from the model for whatever page(testing.html) that contains the directive
						
						personObject: "=", //2-way binding, that allows us to pass the personObj down to the directive
						formattedAddressFn: "&" // poking a hole that allows a function
						// personName: "@", // property: type of hole(local txt-binding)
						// personAddress: "@",
						// personZip: "@"
						
				}
		}
		
});
