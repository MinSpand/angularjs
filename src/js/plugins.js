// Avoid `console` errors in browsers that lack a console.
(function() {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeline', 'timelineEnd', 'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());

// Place any jQuery/helper plugins in here.

/**
	* Created by FreshEyes on 15-10-2016.
	* Used to explore in-page linking and play around
	* with basic js stuff
	*
	* The window.location object can be used to get the current page address (URL) and to redirect the browser to a new
	* The window.location object can be written without the window prefix.
	Some examples:
	
	window.location.href returns the href (URL) of the current page
	window.location.hostname returns the domain name of the web host
	window.location.pathname returns the path and filename of the current page
	window.location.protocol returns the web protocol used (http:// or https://)
	window.location.assign loads a new document
	
	* using 'hashchange' evt listener to get the url
	*/

window.addEventListener("hashchange", function () {
		// modern browsers all support the event, but we have to do the check ;(
		if ('onhashchange' in window) {
				window.onhashchange = function () {
						hashchange(window.location.hash);
				};
		}
		else {
				var wl = this.window.location;  // store the current url
				// Setting a timer to check if the hash has changed
				// and then update the parent iframe
				window.setInterval(function () {
						if (window.location.hash !== wl.hash) { // compare to previous hash
								wl.hash = window.location.hash; // store the current hash value
								hashchange(wl.hash);
								console.log("Hash changed! " + wl.hash + ", " + wl.href + ", "
										+ wl.hostname + ", " + wl.pathname + ", " + wl.protocol);
						}
				}, 100);
				
				console.log("Hash changed! " + wl.hash + ", " + wl.href + ", "
						+ wl.hostname + ", " + wl.pathname + ", " + wl.protocol);
		} //----end if/else
}); //----end addEvt
