/**
	* Created by FreshEyes on 19-10-2016.
	*/
// var myDir = angular.module("myDir", []);

/**
	* Declaring a directive
	* with a fn that returns the dir object
	* @property restrict (allowing non-default types, Attribute, Element, Class, coMment)
	* @property template(Html in ONE string), or templateUrl
	*
	*/
// myDir.directive("searchResult", function() {
//     return {
//         restrict: "AECM",
//         templateUrl: "tpl/directives.html",
//        // replace: true // will generate the appropriate html everywhere this custom element is found : defaults to false
//     }
//
// });

//
//
// myDir.directive("secondDir", function() {
//
// });
//
// myDir.directive("thirdDir", function() {
//
//
// });